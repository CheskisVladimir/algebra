#include "stdafx.h"
#include "CppUnitTest.h"
#include "Matrix.hpp"
#include <memory>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(MatrixTest)
	{
		double d2x2[4] = {
			1., 2.,
			3., 4.
		};

		double dB[2] = {
			3.,
			7.
		};
		
	public:
		
		TEST_METHOD(SetGet)
		{
			Matrix M(2, 3);
			
			Assert::AreEqual(2, M.icount());
			Assert::AreEqual(3, M.jcount());

			M.set(1, 2, 5.);
			Assert::AreEqual(5., M.get(1, 2));

			Assert::AreEqual(5., M.data()[5]);
		}

		TEST_METHOD(SwapRows)
		{
			Matrix M{ 2, 2, d2x2 };
			swapRows(M, 0, 1);

			Assert::AreEqual(3., M.get(0, 0));
			Assert::AreEqual(4., M.get(0, 1));			
			Assert::AreEqual(1., M.get(1, 0));
			Assert::AreEqual(2., M.get(1, 1));
		}

		TEST_METHOD(MinusRow)
		{
			Matrix M{ 2, 2, d2x2 };
			minusRow(M, 1, 0, 0, 3);

			Assert::AreEqual(1., M.get(0, 0));
			Assert::AreEqual(2., M.get(0, 1));
			Assert::AreEqual(0., M.get(1, 0));
			Assert::AreEqual(-2., M.get(1, 1));
		}
		
		TEST_METHOD(ScaleRow)
		{
			Matrix M{ 2, 2, d2x2 };
			scaleRow(M, 0, 1, 3);

			Assert::AreEqual(1., M.get(0, 0));
			Assert::AreEqual(6., M.get(0, 1));
			Assert::AreEqual(3., M.get(1, 0));
			Assert::AreEqual(4., M.get(1, 1));
		}

		TEST_METHOD(MakeIdentityRow)
		{
			Matrix M{ 2, 2, d2x2 };
			Matrix b{ 2, 1, dB };
			makeIdentityRow(M, b, 0);

			Assert::AreEqual(1., M.get(0, 0));
			Assert::AreEqual(2., M.get(0, 1));
			Assert::AreEqual(0., M.get(1, 0));
			Assert::AreEqual(-2., M.get(1, 1));

			Assert::AreEqual(3., b.get(0, 0));
			Assert::AreEqual(-2., b.get(1, 0));
		}

		TEST_METHOD(MakeIdentityRowWithSwap)
		{
			double dM[4] = {
				0., 2.,
				3., 4.
			};
			Matrix M{ 2, 2, dM };
			Matrix b{ 2, 1, dB }; // 3, 7
			makeIdentityRow(M, b, 0);

			Assert::AreEqual(1., M.get(0, 0));
			Assert::AreEqual(4./3., M.get(0, 1), EPSILON);
			Assert::AreEqual(0., M.get(1, 0));
			Assert::AreEqual(2., M.get(1, 1));

			Assert::AreEqual(7./3., b.get(0, 0), EPSILON);
			Assert::AreEqual(3., b.get(1, 0));
		}

		TEST_METHOD(MakeIdentity)
		{
			double dM[4] = {
				0., 2.,
				3., 4.
			};
			Matrix M{ 2, 2, dM };
			Matrix b{ 2, 1, dB }; // 3, 7
			makeIdentity(M, b);

			Assert::AreEqual(1., M.get(0, 0));
			Assert::AreEqual(0., M.get(0, 1), EPSILON);
			Assert::AreEqual(0., M.get(1, 0));
			Assert::AreEqual(1., M.get(1, 1));

			Assert::AreEqual(7. / 3.-2., b.get(0, 0), EPSILON);
			Assert::AreEqual(3. / 2, b.get(1, 0), EPSILON);
		}

		TEST_METHOD(IsIdentity)
		{
			double dE[4] = {
				1., 0.,
				0., 1.
			};
			Matrix E{ 2, 2, dE };
			Matrix b{ 2, 1, dB }; 
			Matrix M{ 2, 2, d2x2 };
			

			Assert::AreEqual(true, isIdentity(E));
			Assert::AreEqual(false, isIdentity(b));
			Assert::AreEqual(false, isIdentity(M));
		}

		TEST_METHOD(Solve)
		{
			Matrix b{ 2, 1, dB };
			Matrix M{ 2, 2, d2x2 };
			Matrix x(2, 1);


			Assert::AreEqual(true, solve(M, b, x));
			Assert::AreEqual(1., x.get(0, 0));
			Assert::AreEqual(1., x.get(1, 0));
		}

		TEST_METHOD(SolveRetFalse)
		{
			double d[4] = {
				1., 2.,
				3., 6.
			};
			Matrix b{ 2, 1, dB };
			Matrix M{ 2, 2, d };
			Matrix x(2, 1);

			Assert::AreEqual(false, solve(M, b, x));
		}
	};
}