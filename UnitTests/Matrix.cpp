#include "stdafx.h"
#include <stdlib.h>
#include"Matrix.hpp"

void swapRows(Matrix& M, int i1, int i2)
{
	for (int j = 0; j < M.jcount(); ++j) {
		double t = M.get(i1, j);
		M.set(i1, j, M.get(i2, j));
		M.set(i2, j, t);
	}
}



void minusRow(Matrix& M, int fromRow, int row, int j0, double coeff)
{
	for (int j = j0; j < M.jcount(); j++) {
		double a = M.get(fromRow, j);
		double b = M.get(row, j);
		M.set(fromRow, j, a - coeff * b);
	}
}

void scaleRow(Matrix& M, int row, int j0, double coeff)
{
	for (int j = j0; j < M.jcount(); j++) {
		M.set(row, j, coeff * M.get(row, j));
	}
}

bool makeIdentityRow(Matrix& M, Matrix& b, int row) 
{
	int i = row;
	double a = 0.;
	for (; i < M.icount(); i++)
	{
		a = M.get(i, row);
		if (std::abs(a) > EPSILON)
		{
			break;
		}
	}

	if (i == M.icount())
		return false;
	if (i != row) {
		swapRows(M, i, row);
		swapRows(b, i, row);
	}

	double coeff = 1. / a;
	scaleRow(M, row, row, coeff);
	scaleRow(b, row, 0, coeff);

	for (i = 0; i < M.icount(); i++) 
	{
		if (i == row)
			continue;

		a = M.get(i, row);
		if (std::abs(a) > EPSILON)
		{
			minusRow(M, i, row, row, a);
			minusRow(b, i, row, 0, a);
		}
	}

	return true;
}

bool makeIdentity(Matrix& M, Matrix& b)
{
	for (int i = 0; i < M.icount(); ++i) {
		if (!makeIdentityRow(M, b, i))
			return false;
	}
	return true;
}

bool isIdentity(Matrix& M) {
	if (M.icount() != M.jcount())
		return false;
	for (int i = 0; i < M.icount(); i++) {
		for (int j = 0; j < M.jcount(); j++) {
			double a = M.get(i, j);
			double expected = i == j ? 1. : 0.;
			if (std::abs(a - expected) > EPSILON)
			{ 
				return false;
			}
		}
	}
	return true;
}

bool solve(Matrix& M, Matrix& b, Matrix& x) {
	if (!makeIdentity(M, b))
		return false;
	for (int i = 0; i < b.icount(); i++) {
		x.set(i, 0, b.get(i, 0));
	}
	return true;
}