#pragma once

#include <memory>

const double EPSILON = 1E-6;

class Matrix 
{
public:
	Matrix(int _icount, int _jcount)
	{
		m_icount = _icount;
		m_jcount = _jcount;
		int N = m_icount * m_jcount;
		if (N > 0)
			m_data = std::make_unique<double[]>((size_t)N);
	}

	Matrix(int _icount, int _jcount, double* _data)
		:Matrix(_icount, _jcount)
	{
		if (_data != nullptr) {
			memcpy(data(), _data, _icount * _jcount * sizeof(double));
		}
	}

	Matrix(const Matrix& M) = delete;
	const Matrix& operator=(const Matrix& M) = delete;

	int icount() const { return m_icount; }
	int jcount() const { return m_jcount; }
	double get(int i, int j) const { return m_data.get()[pos(i, j)]; }
	void set(int i, int j, double value) { m_data.get()[pos(i, j)] = value; }

	double *data()
	{
		return m_data.get();
	}
private:

	int pos(int i, int j) const
	{
		return i * m_jcount + j;
	}

	int m_icount, m_jcount;
	std::unique_ptr<double[]> m_data;
};

void swapRows(Matrix& M, int i1, int i2);
void minusRow(Matrix& M, int fromRow, int row, int j0, double coeff);
void scaleRow(Matrix& M, int row, int j0, double coeff);

bool makeIdentityRow(Matrix& M, Matrix& b, int row);

bool makeIdentity(Matrix& M, Matrix& b);
bool isIdentity(Matrix& M);

bool solve(Matrix& M, Matrix& b, Matrix& x);